﻿using System.Data.Entity.ModelConfiguration;
using Data.Entities;

namespace Data.TableConfigurations
{
    public class IncomingReceiptDetailsTableConfiguration : EntityTypeConfiguration<IncomingReceiptDetails>
    {
        public IncomingReceiptDetailsTableConfiguration()
        {
            HasRequired(x => x.IncomingReceipt).WithMany(x => x.IncomingReceiptDetails);
        }
    }
}