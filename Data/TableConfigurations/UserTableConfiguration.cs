﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.TableConfigurations
{
    public class UserTableConfiguration : EntityTypeConfiguration<User>
    {
        public UserTableConfiguration()
        {
             HasMany(c => c.Roles)
            .WithMany(t => t.Users)
            .Map(m =>
            {
                m.ToTable("UserRoles");
                m.MapLeftKey("UserId");
                m.MapRightKey("RoleId");
            });
        }
    }
}
