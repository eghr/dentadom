﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.TableConfigurations
{
    public class MedicalHistoryTableConfiguration : EntityTypeConfiguration<MedicalHistory>
    {
        public MedicalHistoryTableConfiguration()
        {
            HasMany(m => m.Conditions)
            .WithRequired(c => c.MedicalHistory)
            .HasForeignKey(c => c.MedicalHistoryId);

          
            HasMany(m => m.Medications)
            .WithRequired(Md => Md.MedicalHistory)
            .HasForeignKey(Md => Md.MedicalHistoryId);

            HasMany(m => m.Treatments)
            .WithRequired(t => t.MedicalHistory)
            .HasForeignKey(t => t.MedicalHistoryId);
        }
    }
}
