﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.TableConfigurations
{
    public class AppointmentTableConfiguration : EntityTypeConfiguration<Appointment>
    {
        public AppointmentTableConfiguration ()
        {

            HasRequired(a => a.Patient)
            .WithMany(p => p.Appointments)
            .HasForeignKey(a => a.PatientId)
            .WillCascadeOnDelete(false);


            HasRequired(a => a.User)
            .WithMany(u => u.Appointments)
            .HasForeignKey(a => a.UserId)
            .WillCascadeOnDelete(false);
        }
    }
}
