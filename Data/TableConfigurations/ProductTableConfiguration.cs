﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.TableConfigurations
{
    public class ProductTableConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductTableConfiguration() {
            HasMany(p => p.Suppliers)
            .WithMany(s => s.Products)
            .Map(m =>
            {
                m.ToTable("ProductSupliers");
                m.MapLeftKey("ProductId");
                m.MapRightKey("SupplierId");
            });
        }

    }
}
