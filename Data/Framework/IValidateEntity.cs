﻿using ValidationResult = FluentValidation.Results.ValidationResult;


namespace Data.Framework
{
    public interface IValidateEntity
    {
        ValidationResult Validate();
    }
}
