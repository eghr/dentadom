﻿using System;
using FluentValidation;

namespace Data.Framework
{
    public abstract class ValidateSoftDeleteEntity<TValidator> : ValidateEntity<TValidator>, ISoftDeleteEntity
        where TValidator : IValidator
    {
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public string DeletedReason { get; set; }
    }
}
