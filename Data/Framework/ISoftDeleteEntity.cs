﻿using System;

namespace Data.Framework
{
    public interface ISoftDeleteEntity
    {
        bool IsDeleted { get; set; }
        DateTime? DeletedOn { get; set; }
        string DeletedReason { get; set; }
    }
}
