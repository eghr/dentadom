﻿using System;
using FluentValidation;
using FluentValidation.Results;

namespace Data.Framework
{
    public abstract class ValidateEntity<TValidator> : Entity, IValidateEntity where TValidator : IValidator
    {
        public ValidationResult Validate()
        {
            var validator = Activator.CreateInstance<TValidator>();

            return validator.Validate(this);
        }
    }
}
