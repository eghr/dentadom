﻿using System;
using System.Collections.Generic;
using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class IncomingReceipt: ValidateSoftDeleteEntity<IncomingReceiptValidator>
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public int CreditInvoiceId { get; set; }
        public CreditInvoice CreditInvoice { get; set; }
        public ICollection<IncomingReceiptDetails> IncomingReceiptDetails { get; set; }
    }
}
