﻿using System.Collections.Generic;
using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class InvoiceDetail : ValidateSoftDeleteEntity<InvoiceDetailValidator>
    {
        public int InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
        public int MedicalProcedureId { get; set; }
        public MedicalProcedure MedicalProcedure { get; set; }
        public decimal MedicalProcedurePrice { get; set; }
    }
}
