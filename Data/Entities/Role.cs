﻿using Data.Framework;
using Data.Validations;
using System.Collections.Generic;

namespace Data.Entities
{
    public class Role : ValidateEntity<RoleValidation>
    {
        public string Description { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}