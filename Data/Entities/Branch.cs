﻿using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class Branch : ValidateEntity<BranchValidator>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Tel { get; set; }
    }
}
