﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Validations;
using System.Collections.ObjectModel;

namespace Data.Entities
{
    public class Patient : ValidateSoftDeleteEntity<PatientValidator>
    {
     
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Email { get; set; }
        public virtual List<Phone> Phones { get; set; }
        public virtual List<Appointment> Appointments {get; set;}
        public string CompleteName => $"{Name} {LastName}";
    }
}
