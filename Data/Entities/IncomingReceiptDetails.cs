﻿using System;
using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class IncomingReceiptDetails : ValidateSoftDeleteEntity<IncomingReceiptDetailsValidator>
    {
        public int IncomingReceiptId { get; set; }
        public IncomingReceipt IncomingReceipt { get; set; }
        public decimal Amount { get; set; }
    }
}