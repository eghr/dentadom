﻿using Data.Framework;
using Data.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class MedicalHistory : ValidateSoftDeleteEntity<MedicalHistoryValidatior>
    {
        public int PDI { get; set; } //Periodontal Disease Index()
        public int GI { get; set; }// Gingival Index(GI).
        public int PlI { get; set; }// Plaque Index(PlI)
        public int SHI { get; set; }// Simple hygiene Index(SHI)
        public int PatientId {get; set;}
        public Patient Patient {get; set;}
        public List<Treatment> Treatments {get; set;}
        public List<Condition> Conditions {get; set;}
        public List<Medication> Medications {get; set;}

    }
}
