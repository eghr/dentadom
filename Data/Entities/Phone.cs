﻿using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class Phone : ValidateEntity<PhoneValidator>
    {
        public string Number { get; set; }
        public string Type { get; set; }
        public int Primary { get; set; }
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}