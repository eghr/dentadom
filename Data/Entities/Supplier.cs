﻿using Data.Framework;
using Data.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Supplier : ValidateSoftDeleteEntity<SupplierValidator>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public List<Product> Products { get; set; }
    }
}
