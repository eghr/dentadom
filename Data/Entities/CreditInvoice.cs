﻿using Data.Framework;
using Data.Validations;

namespace Data.Entities
{
    public class CreditInvoice : ValidateSoftDeleteEntity<CreditInvoiceValidator>
    {
        public Invoice Invoice { get; set; }
        public int InvoiceId { get; set; }
        public decimal Balance { get; set; }
    }
}
