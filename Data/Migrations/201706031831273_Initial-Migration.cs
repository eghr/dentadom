namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Tel = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CreditInvoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceId = c.Int(nullable: false),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId)
                .Index(t => t.InvoiceId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PatientId = c.Int(nullable: false),
                        PaymentMethod = c.Int(nullable: false),
                        AppointmentId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Appointments", t => t.AppointmentId)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.PatientId)
                .Index(t => t.AppointmentId);
            
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Description = c.String(),
                        UserId = c.Int(nullable: false),
                        PatientId = c.Int(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        IsConfirm = c.Boolean(nullable: false),
                        IsMessageSent = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastName = c.String(),
                        Birthday = c.DateTime(nullable: false),
                        Email = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                        Type = c.String(),
                        Primary = c.Int(nullable: false),
                        PatientId = c.Int(nullable: false),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Preferences = c.String(),
                        Photo = c.String(),
                        Image = c.Binary(),
                        ImageName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InvoiceDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceId = c.Int(nullable: false),
                        MedicalProcedureId = c.Int(nullable: false),
                        MedicalProcedurePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId)
                .ForeignKey("dbo.MedicalProcedures", t => t.MedicalProcedureId)
                .Index(t => t.InvoiceId)
                .Index(t => t.MedicalProcedureId);
            
            CreateTable(
                "dbo.MedicalProcedures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MedicalProcedureType = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IncomingReceiptDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomingReceiptId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncomingReceipts", t => t.IncomingReceiptId)
                .Index(t => t.IncomingReceiptId);
            
            CreateTable(
                "dbo.IncomingReceipts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Note = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreditInvoiceId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CreditInvoices", t => t.CreditInvoiceId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CreditInvoiceId);
            
            CreateTable(
                "dbo.Conditions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Description = c.String(),
                        MedicalHistoryId = c.Int(nullable: false),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MedicalHistories", t => t.MedicalHistoryId)
                .Index(t => t.MedicalHistoryId);
            
            CreateTable(
                "dbo.MedicalHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PDI = c.Int(nullable: false),
                        GI = c.Int(nullable: false),
                        PlI = c.Int(nullable: false),
                        SHI = c.Int(nullable: false),
                        PatientId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Medications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        MedicalHistoryId = c.Int(nullable: false),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MedicalHistories", t => t.MedicalHistoryId)
                .Index(t => t.MedicalHistoryId);
            
            CreateTable(
                "dbo.Treatments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                        Result = c.String(),
                        Attachment = c.Binary(),
                        AttachmentName = c.String(),
                        MedicalHistoryId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MedicalHistories", t => t.MedicalHistoryId)
                .Index(t => t.MedicalHistoryId);
            
            CreateTable(
                "dbo.InventoryTransactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Brand = c.String(),
                        UnitCost = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentMehotds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeletedOn = c.DateTime(),
                        DeletedReason = c.String(),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ProductSupliers",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        SupplierId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.SupplierId })
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoryTransactions", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductSupliers", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.ProductSupliers", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Treatments", "MedicalHistoryId", "dbo.MedicalHistories");
            DropForeignKey("dbo.MedicalHistories", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Medications", "MedicalHistoryId", "dbo.MedicalHistories");
            DropForeignKey("dbo.Conditions", "MedicalHistoryId", "dbo.MedicalHistories");
            DropForeignKey("dbo.IncomingReceiptDetails", "IncomingReceiptId", "dbo.IncomingReceipts");
            DropForeignKey("dbo.IncomingReceipts", "UserId", "dbo.Users");
            DropForeignKey("dbo.IncomingReceipts", "CreditInvoiceId", "dbo.CreditInvoices");
            DropForeignKey("dbo.CreditInvoices", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "UserId", "dbo.Users");
            DropForeignKey("dbo.Invoices", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.InvoiceDetails", "MedicalProcedureId", "dbo.MedicalProcedures");
            DropForeignKey("dbo.InvoiceDetails", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "AppointmentId", "dbo.Appointments");
            DropForeignKey("dbo.Appointments", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.Appointments", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Phones", "PatientId", "dbo.Patients");
            DropIndex("dbo.ProductSupliers", new[] { "SupplierId" });
            DropIndex("dbo.ProductSupliers", new[] { "ProductId" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.InventoryTransactions", new[] { "ProductId" });
            DropIndex("dbo.Treatments", new[] { "MedicalHistoryId" });
            DropIndex("dbo.Medications", new[] { "MedicalHistoryId" });
            DropIndex("dbo.MedicalHistories", new[] { "PatientId" });
            DropIndex("dbo.Conditions", new[] { "MedicalHistoryId" });
            DropIndex("dbo.IncomingReceipts", new[] { "CreditInvoiceId" });
            DropIndex("dbo.IncomingReceipts", new[] { "UserId" });
            DropIndex("dbo.IncomingReceiptDetails", new[] { "IncomingReceiptId" });
            DropIndex("dbo.InvoiceDetails", new[] { "MedicalProcedureId" });
            DropIndex("dbo.InvoiceDetails", new[] { "InvoiceId" });
            DropIndex("dbo.Phones", new[] { "PatientId" });
            DropIndex("dbo.Appointments", new[] { "PatientId" });
            DropIndex("dbo.Appointments", new[] { "UserId" });
            DropIndex("dbo.Invoices", new[] { "AppointmentId" });
            DropIndex("dbo.Invoices", new[] { "PatientId" });
            DropIndex("dbo.Invoices", new[] { "UserId" });
            DropIndex("dbo.CreditInvoices", new[] { "InvoiceId" });
            DropTable("dbo.ProductSupliers");
            DropTable("dbo.UserRoles");
            DropTable("dbo.PaymentMehotds");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Products");
            DropTable("dbo.InventoryTransactions");
            DropTable("dbo.Treatments");
            DropTable("dbo.Medications");
            DropTable("dbo.MedicalHistories");
            DropTable("dbo.Conditions");
            DropTable("dbo.IncomingReceipts");
            DropTable("dbo.IncomingReceiptDetails");
            DropTable("dbo.MedicalProcedures");
            DropTable("dbo.InvoiceDetails");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Phones");
            DropTable("dbo.Patients");
            DropTable("dbo.Appointments");
            DropTable("dbo.Invoices");
            DropTable("dbo.CreditInvoices");
            DropTable("dbo.Branches");
        }
    }
}
