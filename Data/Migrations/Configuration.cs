using System.Globalization;
using Domain.Global.Enums;

namespace Data.Migrations
{
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Data.DentaDomContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DentaDomContext context)
        {
            //all seeds put in here are for testing 
            var medicalProcedures = new List<MedicalProcedure>
            {
                new MedicalProcedure
                {
                    Id=1,
                    Description = "Extraccion de Muela",
                    Price = 1500
                },
                 new MedicalProcedure
                {
                     Id=2,
                    Description = "Instalacion Brackets",
                    Price = 6700
                }
            };
            foreach (var med in medicalProcedures) {
                context.Set<MedicalProcedure>().AddOrUpdate(med);
            }
            context.SaveChanges();

            var roles = new List<Role>();
            roles.Add(new Role { Id = 1, Code = "001", Description = "Administrador" });
            roles.Add(new Role { Id = 2, Code = "002", Description = "Odontologo" });
            roles.Add(new Role { Id = 3, Code = "003", Description = "Secretaria" });
            roles.Add(new Role { Id = 4, Code = "004", Description = "Asistente Dental" });

            foreach (var rol in roles)
            {
                context.Set<Role>().AddOrUpdate(r => r.Id, rol);
            }

            context.SaveChanges();


            var branch = new Branch
            {
                Id=1,
                Name = "Consultorio Grupo Dental Polanco",
                Address1 = "Ave. Bolivar, #241, apt. 202",
                Address2 = "La Julia, Santo Domingo, D.N.",
                Tel = "(809) 533-6821"
            };
            
            context.Set<Branch>().AddOrUpdate(b=> b.Id,branch);

        var users = new List<User> {
               new User
                {
                            Id=1,
                            Name ="Enmanuel",
                            LastName = "Hernandez",
                            UserName = "EHernandez",
                            Password ="12345",
                            Photo ="/Images/enmanuel.png",
                            Roles = new Collection<Role>()
                            {
                                roles.ElementAt(0),
                                roles.ElementAt(1)
                            }
                },
                 new User
                 {
                            Id =2,
                            Name ="Gary",
                            LastName = "Cordero",
                            UserName = "GCordero",
                            Password ="12345",
                            Photo ="/Images/gary.png",
                            Roles = new Collection<Role>()
                            {
                                roles.ElementAt(0),
                                roles.ElementAt(1)
                            }
                },
                  new User
                  {
                            Id=3,
                            Name ="Gabriel",
                            LastName = "Mendez",
                            UserName = "GMendez",
                            Password ="12345",
                            Photo ="/Images/gabriel.png",
                            Roles = new Collection<Role>()
                            {
                                roles.ElementAt(3)
                            }
                },
                new User
                {
                        Id=4,
                        Name ="Anabel",
                        LastName = "Yazal",
                        UserName = "AYazal",
                        Password ="12345",
                        Photo ="/Images/anabel.png",
                        Roles = new Collection<Role>()
                            {
                                roles.ElementAt(2)
                            }
                },
                new User
                {
                        Id=5,
                        Name ="Juab",
                        LastName = "Zalazar",
                        UserName = "JZ",
                        Password ="12345",
                        Photo ="/Images/Juab.png",
                        Roles = new Collection<Role>()
                            {
                                roles.ElementAt(1)
                            }
                }

            };
            foreach (var user in users)
            {
                context.Set<User>().AddOrUpdate(u => u.Id, user);
            }

            var phones = new List<Phone>()
            {
                new Phone()
                {
                    Id=1,
                    Number = "809-322-9443",
                    Type = "Celular",
                    Primary =1
                },
                 new Phone()
                {
                    Id=2,
                    Number = "809-322-7777",
                    Type = "Casa",
                    Primary =1
                },
                  new Phone()
                {
                    Id=3,
                    Number = "809-322-5555",
                    Type = "Celular",
                    Primary =1
                }
            };

            var patients = new List<Patient>()
            {
                new Patient()
                {
                    Id = 1,
                    Name ="Starling",
                    LastName ="De Leon",
                    Birthday = Convert.ToDateTime("03-07-1991"),
                    Email = "StarDLeon@gmail.com",
                    Phones = new List<Phone>()
                        {
                            phones.ElementAt(0)
                        }
                },
                new Patient()
                {
                    Id = 2,
                    Name ="Francisco",
                    LastName ="Vasquez",
                    Birthday = Convert.ToDateTime("03-07-1991"),
                    Email = "FVasquez16@gmail.com",
                     Phones = new List<Phone>()
                    {
                         phones.ElementAt(1)
                    }
                },
                new Patient()
                {
                    Id = 3,
                    Name ="Vladimir",
                    LastName ="Vasquez",
                    Birthday = Convert.ToDateTime("03-07-1991"),
                    Email = "VVasquez04@gmail.com",
                    Phones = new List<Phone>()
                    {
                            phones.ElementAt(2)
                    }
                }

        };
        foreach (var patient in patients)
        {
            context.Set<Patient>().AddOrUpdate(p => p.Id, patient);
        }
        }
    }
}
