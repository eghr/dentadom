﻿using Data.Entities;
using FluentValidation;

namespace Data.Validations
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("El nombre de usuario no debe estar vacio");
        }
    }
}
