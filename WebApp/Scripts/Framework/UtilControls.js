﻿$('document').ready(function() {
    // Select2
    $(".select2")
        .select2({
            placeholder: "Click para Seleccionar...",
            allowClear: true,
            dropdownCssClass :"col-md-12"
        });

    $(".select2-ajax")
        .each(function () {
            var $this = $(this);

            console.log("ajax url", $this.data("ajaxurl"));

            var endpointUrl = $this.data("ajaxurl");

            $this.select2({
                placeholder: "",
                allowClear: true,
                ajax: {
                    url: endpointUrl,
                    dataType: "json",
                    delay: 250,
                    data: function (params) {
                        return {
                            name: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data,
                                function (obj) {
                                    return { id: obj.id, text: obj.text };
                                })
                        };
                    }
                }
            });
        });

    // DatePicker
    //$(".datepicker")
    //    .datepicker({
    //        todayBtn: "linked",
    //        keyboardNavigation: false,
    //        forceParse: false,
    //        calendarWeeks: true,
    //        autoclose: true
    //    });
});