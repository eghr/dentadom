﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AppService.Framework;
using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Hosting;
using Hangfire;
using Microsoft.Owin;
using WebApp;
using Owin;
using WebApp.Helpers;

[assembly: OwinStartup(typeof(MvcApplication))]
namespace WebApp
{
   
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Autofac Configuration
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
            builder.RegisterModule(new DentaDomApplicationModule());
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            //Recurring Jobs
            GlobalConfiguration.Configuration.UseSqlServerStorage("DentaDomConnString");
            RecurringJob.AddOrUpdate(
            () => new MessageQuery().SendMessage(),
            Cron.MinuteInterval(30));

        }

        public void Configuration(IAppBuilder app)
        {
            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}
