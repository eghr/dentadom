﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Global.Dto;

namespace WebApp.ViewModels.InvoiceReport
{
    public class InvoiceReportViewModel
    {
        public IEnumerable<InvoiceReportDto> InvoiceReportDto { get; set; }
        [Display(Name = "Desde Fecha")]
        public string FromDate { get; set; }

        [Display(Name = "Hasta Fecha")]
        public string ToDate { get; set; }
    }
}