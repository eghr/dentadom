﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Data.Entities;
using Domain.Global.Enums;

namespace WebApp.ViewModels.User
{
    public class NewUserViewModel
    {
        [Display (Name = "Nombres")]
        [Required(ErrorMessage = "El/Los Nombre es Requerido")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "El/Los Apellidos es Requerido")]
        public string LastName { get; set; }

        [Display(Name = "Nombre de Usuario")]
        [Required(ErrorMessage = "El Nombre de Usuario es Requerido")]
        public string UserName { get; set; }

        [Display(Name = "Contraseña")]
        [Required(ErrorMessage = "La Contraseña es Requerida")]
        public string Password { get; set; }

        [Display(Name = "Confirmar Contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "La Contraseña no coincide")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Seleccione una foto")]
        public HttpPostedFileBase Photo { get; set; }

        public SelectList RoleType { get; set; }

        [Display(Name = "Rol")]
        [Required(ErrorMessage = "El Rol es Requerido")]
        public ICollection<RoleTypeEnum> SelectedRoleType { get; set; }
    }
}