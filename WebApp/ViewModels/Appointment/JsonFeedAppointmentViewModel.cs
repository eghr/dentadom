﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class JsonFeedAppointmentViewModel
    {
        public int Id { get; set;}
        public string StartDate { get; set;}
        public string EndDate { get; set;}
        public string Description { get; set; }
        public int UserId { get; set;}
        public bool IsUserDeleted { get; set; }
        public int PatientId { get; set;}
        public string PatientName { get; set; }
        public bool IsPaid { get; set; }

    }
}