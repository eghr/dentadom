﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.ViewModels;

namespace WebApp.ViewModels
{
    public class JsonFeedProdcutViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string SupplierIds { get; set; }
        public string Suppliers { get; set; }
        public double UnitCost { get; set; }
        public int Quantity { get; set; }

    
    }
}