﻿using AppService.Framework;

namespace WebApp.ViewModels
{
    public class ErrorViewModel
    {
        public ResponseModel ResponseModel { get; set; }

        public string Messages { get; set; }

        public string Referrer { get; set; }
    }
}