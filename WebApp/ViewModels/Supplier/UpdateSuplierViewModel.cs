﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class UpdateSuplierViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Compañia")]
        public string Name { get; set; }
        [Display(Name = "Direccion")]
        public string Address { get; set; }
        [Display(Name = "Telefono")]
        public string Phone { get; set; }
    }
}