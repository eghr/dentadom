﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class JsonFeedInvenotryTransaccionViewModel
    {
        public int Id { get; set; }
        public string CreatedOn { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int Type { get; set; }
        public string TypeDescripcion { get; set; }
        public string ProductName { get; set; }
       
    }
}