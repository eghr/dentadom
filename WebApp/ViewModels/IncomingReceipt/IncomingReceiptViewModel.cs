﻿using System;
using System.Collections.Generic;
using Data.Entities;

namespace WebApp.ViewModels.IncomingReceipt
{
    public class IncomingReceiptViewModel
    {
        public IEnumerable<CreditInvoice> CreditInvoices { get; set; } = new List<CreditInvoice>();
    }
}