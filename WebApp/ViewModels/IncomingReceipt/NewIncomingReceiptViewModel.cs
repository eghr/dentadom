﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Data.Entities;

namespace WebApp.ViewModels.IncomingReceipt
{
    public class NewIncomingReceiptViewModel
    {
        public int CreditInvoiceId { get; set; }
        [Display(Name = "Numero de Factura")]
        public int? InvoiceNumber { get; set; }
        [Display(Name = "Paciente")]
        public string CustomerName { get; set; }
        [Display(Name = "Balance a la fecha")]
        public decimal CurrentBalance { get; set; }

        [Display(Name = "Importe")]
        [Range(1, int.MaxValue, ErrorMessage = "El Importe debe ser mayor a 0")]
        [Required(ErrorMessage = "Debe digitar un monto")]
        public decimal Amount { get; set; }
        [Display(Name = "Comentario")]
        public string Comment { get; set; }
        [Display(Name = "Detalle Factura")]
        public List<MedicalProcedure> MedicalProcedures { get; set; }
    }
}