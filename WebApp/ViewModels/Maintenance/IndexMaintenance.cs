﻿using System.Web.Mvc;

namespace WebApp.ViewModels.MaintenanceViewModel
{
    public class IndexMaintenance
    {
        public string Title { get; set; }
        public MvcHtmlString IndexBody { get; set; }
        public string NewButtonUrl { get; set; }
    }
}