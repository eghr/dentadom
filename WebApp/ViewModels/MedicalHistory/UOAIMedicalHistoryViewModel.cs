﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class UOAIMedicalHistoryViewModel
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        [Display(Name = "Indice de Periodontitis")]
        public int PDI { get; set; }
        [Display(Name = "Indice Gingival")]
        public int GI { get; set; }
        [Display(Name = "Indice de Placa")]
        public int PlI { get; set; }
        [Display(Name = "Indice de Higiene")]
        public int SHI { get; set; }
    }
}