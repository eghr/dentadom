﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApp.ViewModels
{
    public class TreatmentViewModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public int MedicalHistoryId { get; set; }
        [Display(Name = "Adjunto")]
        public HttpPostedFileBase Attachment { get; set; }
        public string AttachmentName { get; set; }
    }
}