﻿using AppService.Services;
using Data;
using Data.Entities;
using Data.Framework;
using MessageSender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebApp.Helpers
{
    public class MessageQuery : IDisposable
    {
        private bool disposedValue = false;
        private IAppointmentService _appointmentService;
        
        public MessageQuery()
        {
            _appointmentService = new AppointmentService(new UnitOfWork(new DentaDomContext()));
        }
        public async Task SendMessage()
        {
            var response = await _appointmentService.GetAppointmentsByDateInterVal(DateTime.Now,DateTime.Now.AddHours(1));
            if (response.ExcecutedSuccesfully)
            {
                await DentadomMessageManager.sendReminders(response.Data);
                foreach(var appointment in response.Data)
                {
                    await _appointmentService.AutoMessaged(appointment.Id);
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _appointmentService.Dispose();
                }

                disposedValue = true;
            }
        }

     
        public void Dispose()
        {
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
     

    }
}