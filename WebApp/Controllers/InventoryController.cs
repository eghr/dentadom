﻿using AppService.Framework;
using AppService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.ViewModels;
using WebApp.ViewModels.Product;

namespace WebApp.Controllers
{
    //[AuthenticationRequired]
    public class InventoryController : Controller
    {

        private readonly ISupplierService _supplierService;
        private readonly IProductService _productService;
        private readonly IInventoryTransactionService _inventoryTransactionService;

        public InventoryController(ISupplierService supplierService, IProductService productService, IInventoryTransactionService inventoryTrasnactionService)
        {
            _supplierService = supplierService;
            _productService = productService;
            _inventoryTransactionService = inventoryTrasnactionService;
        }
        // GET: Inventory
        public ActionResult Materials()
        {
          
            return View();
        }
        public ActionResult Suppliers()
        {

            return View();
        }
        public ActionResult Transactions()
        {
           
            return View();
        }

        public async Task<JsonResult> ProductsFeed()
        {
            var response = await _productService.GetProducts();
            var products = new List<JsonFeedProdcutViewModel>();
            foreach(var data in response.Data)
            {
                JsonFeedProdcutViewModel productfeed = new JsonFeedProdcutViewModel();
                productfeed.Id = data.Id;
                productfeed.Name = data.Name;
                productfeed.Brand = data.Brand;
                productfeed.Quantity = data.Quantity;
                productfeed.UnitCost = data.UnitCost;
                for(int i = 0; i < data.Suppliers.Count; i++)
                {
                    if (data.Suppliers.ElementAt(i).IsDeleted)
                    {
                        continue;
                    }
                    productfeed.Suppliers += data.Suppliers.ElementAt(i).Name;
                    productfeed.SupplierIds += data.Suppliers.ElementAt(i).Id;
                    if ((i+1) != data.Suppliers.Count)
                    {
                        productfeed.Suppliers += "<br/>";
                        productfeed.SupplierIds += ",";
                    }
                }

                products.Add(productfeed);
            }
            return Json(products, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> CreateProduct(CreateProductViewModel product)
        {
            var response = await _productService.CreateProduct(product.Name,product.Brand,product.SupplierId,product.UnitCost,product.Quantity);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateProduct(ProductViewModel product)
        {
            var response = await _productService.UpdateProduct(product.Id,product.Name, product.Brand, product.SupplierId, product.UnitCost);
            return Json(response.ExcecutedSuccesfully, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> RemoveProduct(InventoryTransactionViewModel inventoryTransation)
        {
            var response = await _productService.RemoveProduct(inventoryTransation.ProductId, inventoryTransation.Quantity, inventoryTransation.Type);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AddProduct(InventoryTransactionViewModel inventoryTransation)
        {
            var response = await _productService.AddProduct(inventoryTransation.ProductId, inventoryTransation.Quantity, inventoryTransation.Type);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SuppliersFeed()
        {
            var response = await _supplierService.GetSuppliers();
            var suppliers = new List<JsonFeedSupplierViewModel>();
            if (response.ExcecutedSuccesfully)
            {
                foreach (var supplier in response.Result)
                {
                    suppliers.Add(new JsonFeedSupplierViewModel()
                    {
                        Id = supplier.GetType().GetProperty("Id").GetValue(supplier),
                        Name = supplier.GetType().GetProperty("Name").GetValue(supplier),
                        Phone = supplier.GetType().GetProperty("Phone").GetValue(supplier),
                        Address = supplier.GetType().GetProperty("Address").GetValue(supplier)
                    });
                }
            }
            return Json(suppliers, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CreateSupplier(CreateSupplierViewModel supplier)
        {
            var response = await _supplierService.CreateSupplier(supplier.Name, supplier.Phone, supplier.Address);
            var suppliers = new UpdateSuplierViewModel();
            if (response.ExcecutedSuccesfully)
            { 
                suppliers = new UpdateSuplierViewModel()
                {
                    Id= response.Result.GetType().GetProperty("Id").GetValue(response.Result,null),
                    Name = supplier.Name,
                    Address = supplier.Address,
                    Phone = supplier.Phone
                };
            }
            return Json(suppliers, JsonRequestBehavior.AllowGet);
        }
        
        public async Task<JsonResult> SoftDeleteSupplier(int supplierId =0)
        {
            var response = await _supplierService.SoftDeleteSupplier(supplierId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateSupplier(UpdateSuplierViewModel supplier)
        {

            var response = await _supplierService.UpdateSupplier(supplier.Id, supplier.Name, supplier.Phone, supplier.Address);
            if (response.ExcecutedSuccesfully)
            {
                supplier = new UpdateSuplierViewModel();
            }
            return Json(supplier, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> InventoryTransactionFeed()
        {
            var response = await _inventoryTransactionService.GetInventoryTransaction();
            var itransactions = new List<JsonFeedInvenotryTransaccionViewModel>();
            if (response.ExcecutedSuccesfully)
            {
                foreach (var it in response.Result)
                {
                    var product = it.GetType().GetProperty("Product").GetValue(it, null);
                    itransactions.Add(new JsonFeedInvenotryTransaccionViewModel()
                    {
                        Id = it.GetType().GetProperty("Id").GetValue(it,null),
                        CreatedOn = (it.GetType().GetProperty("CreatedOn").GetValue(it, null)).ToString("dd'/'MM'/'yyyy"),
                        ProductId = it.GetType().GetProperty("ProductId").GetValue(it, null),
                        ProductName = product.Name + " " + product.Brand,
                        Quantity = it.GetType().GetProperty("Quantity").GetValue(it, null),
                        Type = (int) it.GetType().GetProperty("Type").GetValue(it, null),
                        TypeDescripcion = (((int)it.GetType().GetProperty("Type").GetValue(it, null)) == 1 ? "Salida" : "Entrada")
                    });
                   
                }
            }
            return Json(itransactions, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> RollBackTransaction (int inventoryTransactionId)
        {
            var response = await _inventoryTransactionService.RollBackTransaction(inventoryTransactionId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateTransaction(UpdateInventoryTransactionViewModel invTransation)
        {
            var response = await _inventoryTransactionService.UpdateTransaction(invTransation.Id, invTransation.Type, invTransation.Quantity);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}