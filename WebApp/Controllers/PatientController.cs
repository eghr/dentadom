﻿using AppService.Framework;
using AppService.Services;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [AuthenticationRequired]
    public class PatientController : BaseController 
    {
        private readonly IPatientService _patientService;
        private readonly IPhoneService _phoneService;

        public PatientController(IPatientService patientService, IPhoneService phoneService)
        {
            _patientService = patientService;
            _phoneService = phoneService;
        }
        // GET: Patient
        public ActionResult SearchPatient(int id = 0)
        {
            
            return View();
        }

        public async Task<JsonResult> UpdatePatient(UpdatePatientViewModel patient)
        {
            var response = await _phoneService.updatePhone(patient.Id, patient.PhoneNumber, patient.PhoneType);
            var response1 = await _patientService.UpdatePatient(patient.Id, patient.Name, patient.LastName, patient.Birthday, patient.PhoneNumber, patient.PhoneType, patient.Email);
            return Json(patient,JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SetPatient(CreatePatientViewModel patient )
        {
            ResponseModel response = await _patientService.CreatePatient(patient.Name,patient.LastName,patient.Birthday,patient.PhoneNumber,patient.PhoneType,patient.Email);
            if (response.ExcecutedSuccesfully)
            {
                patient.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
            }
            return Json(patient, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PatientsFeed( )
        {
            var respons = await _patientService.GetPatients();
            var patients = new List<JsonFeedPatientViewModel>();
            if(respons.ExcecutedSuccesfully) {
                foreach (var patient in respons.Result)
                {
                    JsonFeedPatientViewModel selectedPatients = new JsonFeedPatientViewModel()
                    {
                        Id = patient.GetType().GetProperty("Id").GetValue(patient, null),
                        Name = patient.GetType().GetProperty("Name").GetValue(patient, null),
                        LastName = patient.GetType().GetProperty("LastName").GetValue(patient, null),
                        Birthday = (patient.GetType().GetProperty("Birthday").GetValue(patient, null)).ToString("dd'/'MM'/'yyyy"),
                        Email = patient.GetType().GetProperty("Email").GetValue(patient, null)
                    };
                    var phones = patient.GetType().GetProperty("Phones").GetValue(patient, null);
                    foreach (var phone in phones)
                    {
                        if (phone.GetType().GetProperty("Primary").GetValue(phone, null) == 1)
                        {
                            selectedPatients.PhoneNumber = phone.GetType().GetProperty("Number").GetValue(phone, null);
                            selectedPatients.PhoneType  = phone.GetType().GetProperty("Type").GetValue(phone, null);
                        }

                    }

                    patients.Add(selectedPatients);
                }
            }
            return Json(patients, JsonRequestBehavior.AllowGet);
        }
    }
}