﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AppService.Services;
using Data.Entities;
using Domain.Global.Enums;
using Domain.Global.Extensions;
using WebApp.Helpers;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [AuthenticationRequired]
    public class InvoiceController : BaseController
    {
        private readonly IAppointmentService _appointmentService;
        private readonly IMedicalProcedureService _medicalProcedureService;
        private readonly IUserService _userService;
        private readonly IInvoiceService _invoiceService;
     
        public InvoiceController(IAppointmentService appointmentService, IMedicalProcedureService medicalProcedureService, IUserService userService, IInvoiceService invoiceService)
        {
            _appointmentService = appointmentService;
            _medicalProcedureService = medicalProcedureService;
            _userService = userService;
            _invoiceService = invoiceService;
        }

        [ReferrOnly]
        public async Task<ActionResult> Invoice(int id)
        {
            var appointmentResult = await _appointmentService.GetAppoimentById(id);
            var medicalProcedureResult = await _medicalProcedureService.GetAllAsync();
            var branch = await _userService.GetBranchAsync();

            if (!appointmentResult.ExcecutedSuccesfully)
            {
               return RedirectToHomeWithError(appointmentResult); // ModalWithError(appointmentResult);
            }
            var viewModel = new InvoiceViewModel
            {
                Appointment = appointmentResult.Data,
                AppointmentId = appointmentResult.Data.Id,
                Patient = appointmentResult.Data.Patient,
                PatientId = appointmentResult.Data.PatientId,
                DentistName = appointmentResult.Data.User.CompleteName,
                MedicalProcedures = new SelectList(medicalProcedureResult.Data,"Id", "DescriptionAndPrice"),
                Branch = branch.Data,
                PaymentMethods = new SelectList(PaymentMethod.Cash.GetAllDiplayNames(),"Id", "DisplayName")
                
            };
            
            return View(viewModel);
        }
      
        public async Task<JsonResult> CancelInvoice(int appointmentId)
        {
            var result = await _invoiceService.DeleteInvoiceByAppointmentId(appointmentId);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> CreateInvoice(InvoiceViewModel data)
        {
            if (!ModelState.IsValid) return Json(ModelStateConverter.GetServiceResult(ModelState), JsonRequestBehavior.AllowGet);

            var invoiceDetailList = new List<InvoiceDetail>();

            for (var i = 0; i < data.procedureList.Length; i++)
            {
                invoiceDetailList.Add(new InvoiceDetail
                {
                    MedicalProcedurePrice = data.priceList[i],
                    MedicalProcedureId = data.procedureList[i]
                });
            }

            var invoice = new Invoice
            {
                UserId = SessionHelper.GetUser(),
                PaymentMethod = (PaymentMethod)data.PaymentMethodSelected,
                PatientId = data.PatientId,
                InvoiceDetails = invoiceDetailList,
                AppointmentId = data.AppointmentId,
                Discount = data.Discount
            };

            var result = await _invoiceService.CreateInvoiceAsync(invoice);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> InvoicePrint(int id)
        {
            var result = await _invoiceService.GetInvoiceById(id);

            return View(result.Data);
        }
        public async Task<ActionResult> InvoiceReprint(int id)
        {
            var result = await _invoiceService.GetInvoiceByAppointmentId(id);
            if (result.ExcecutedSuccesfully)
            {
                return RedirectToAction("InvoicePrint", "Invoice", new { id = result.Data.Id });
            }
            else {
                return RedirectToHomeWithError(result);
            }
        }
    }
}