﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AppService.Framework;
using WebApp.ViewModels;
using WebGrease.Css.Extensions;

namespace WebApp.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly IDisposable[] _disposables;

        protected BaseController(params IDisposable[] disposables)
        {
            _disposables = disposables;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _disposables != null && _disposables.Length > 0)
            {
                _disposables.ForEach(x => x.Dispose());
            }

            base.Dispose(disposing);
        }
        public ActionResult ModalWithError(ResponseModel responseModel)
        {
            var errorViewModel = new ErrorViewModel
            {
                ResponseModel = responseModel
            };

            return PartialView("_ErrorModal", errorViewModel);
        }

        public ActionResult RedirectToHomeWithError(ResponseModel responseModel)
        {
            var errorViewModel = new ErrorViewModel
            {
                ResponseModel = responseModel
            };

           return RedirectToAction("ErrorPage", "Home", new { Referrer = Request.UrlReferrer, Messages = responseModel.Messages.FirstOrDefault()});
        }

        public FileStreamResult FileAsStreamResult(byte[] fileAsByteArray)
        {
            Response.Clear();
            MemoryStream ms = new MemoryStream(fileAsByteArray);
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=DD-Factura.pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);

            return new FileStreamResult(ms, "application/pdf");
        }
    }
}