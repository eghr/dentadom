﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AppService.Services;
using Data.Entities;
using WebApp.Helpers;
using WebApp.ViewModels.IncomingReceipt;
using WebApp.ViewModels.Maintenance;

namespace WebApp.Controllers
{
    public class IncomingReceiptController : BaseController
    {
        private readonly IInvoiceService _invoiceService;
        private readonly IIncomingReceiptService _iiIncomingReceiptService;

        public IncomingReceiptController(IInvoiceService invoiceService, IIncomingReceiptService iiIncomingReceiptService)
        {
            _invoiceService = invoiceService;
            _iiIncomingReceiptService = iiIncomingReceiptService;
        }
        // GET: IncomingReceipt
        public async Task<ActionResult> Index()
        {
            var creditInvoices = await _invoiceService.GetAllCreditInvoice();

            var viewModel = new IncomingReceiptViewModel();
            viewModel.CreditInvoices = creditInvoices.Data;
          
            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> New(int creditInvoiceId)
        {
            var incomingReceipt = await _invoiceService.GetCreditInvoiceById(creditInvoiceId);
            var viewModel = new NewIncomingReceiptViewModel();

            if (incomingReceipt?.Data != null)
            {
                viewModel.CustomerName = incomingReceipt.Data.Invoice.Patient.CompleteName;
                viewModel.CurrentBalance = incomingReceipt.Data.Balance;
                viewModel.MedicalProcedures = incomingReceipt.Data.Invoice.InvoiceDetails?
                    .Select(x => x.MedicalProcedure).ToList();
                viewModel.CreditInvoiceId = creditInvoiceId;
                viewModel.InvoiceNumber = incomingReceipt.Data?.Invoice?.Id;
            }

            var modalViewModel = new ModalGenericViewModel
            {
                Icon = "fa fa-file",
                Title = "Crear recibo de Ingreso",
                FormAction = Url.Action("New", "IncomingReceipt"),
                FormControls = "~/Views/IncomingReceipt/_New.cshtml",
                FormViewModel = viewModel,
                UseAntiForgeryToken = false,
                FormId = "frm-new-receipt",
                FormMethod = FormMethod.Post
            };

            return PartialView("~/Views/Maintenance/_ModalGeneric.cshtml", modalViewModel);
        }

        [HttpPost]
        public async Task<JsonResult> New(NewIncomingReceiptViewModel newIncomingReceiptViewModel)
        {
            if (!ModelState.IsValid) return Json(ModelStateConverter.GetServiceResult(ModelState), JsonRequestBehavior.AllowGet);

            var incomingReceiptToCreate = new IncomingReceipt
            {
                UserId = SessionHelper.GetCurrentUser().Id,
                Amount = newIncomingReceiptViewModel.Amount,
                CreditInvoiceId = newIncomingReceiptViewModel.CreditInvoiceId
            };

            var result = await _iiIncomingReceiptService.CreateNewAsync(incomingReceiptToCreate);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}