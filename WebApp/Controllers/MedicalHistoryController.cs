﻿using AppService.Framework;
using AppService.Services;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Helpers;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    [AuthenticationRequired]
    public class MedicalHistoryController : BaseController
    {

        private readonly IMedicalHistoryService _medicalHistoryService;
        private readonly IConditionService _conditionService;
        private readonly IMedicationServise _medicationService;
        private readonly ITreatmentService _treatmentService;

        public MedicalHistoryController(IMedicalHistoryService medicalHistoryService, IConditionService conditionService, IMedicationServise medicationService, ITreatmentService treatmentService)
        {
            _medicalHistoryService = medicalHistoryService;
            _conditionService = conditionService;
            _medicationService = medicationService;
            _treatmentService = treatmentService;
        }

        // GET: MedicalHistory//This id is PatientId
        [ReferrOnly]
        public async  Task<ActionResult> MedicalHistory(int id = 0)
        {
            ModelState.Clear();
            if (id > 0)
            {
                var response = await _medicalHistoryService.GetorCreateMedicalByPatientId(id);
                var medical = MedicalHistoryViewModel.getMedical();
                if (response.Result != null)
                {
                    medical.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
                    medical.PDI = response.Result.GetType().GetProperty("PDI").GetValue(response.Result, null);
                    medical.GI = response.Result.GetType().GetProperty("GI").GetValue(response.Result, null);
                    medical.PlI = response.Result.GetType().GetProperty("PlI").GetValue(response.Result, null);
                    medical.SHI = response.Result.GetType().GetProperty("SHI").GetValue(response.Result, null);
                    medical.PatientId = response.Result.GetType().GetProperty("PatientId").GetValue(response.Result, null);
                    //fetching the patient
                    var patient = response.Result.GetType().GetProperty("Patient").GetValue(response.Result, null);
                    if (patient != null)
                    {
                        medical.Patient = new UpdatePatientViewModel()
                        {
                            Id = patient.GetType().GetProperty("Id").GetValue(patient, null),
                            Name = patient.GetType().GetProperty("Name").GetValue(patient, null),
                            LastName = patient.GetType().GetProperty("LastName").GetValue(patient, null),
                            Birthday = (patient.GetType().GetProperty("Birthday").GetValue(patient, null)).ToString("dd'/'MM'/'yyyy"),
                            Email = patient.GetType().GetProperty("Email").GetValue(patient, null),
                        };

                        var phones = patient.GetType().GetProperty("Phones").GetValue(patient, null);
                        foreach (var phone in phones)
                        {
                            if (phone.GetType().GetProperty("Primary").GetValue(phone, null) == 1)
                            {
                                medical.Patient.PhoneNumber = phone.GetType().GetProperty("Number").GetValue(phone, null);
                                medical.Patient.PhoneType = phone.GetType().GetProperty("Type").GetValue(phone, null);
                            }
                        }
                    }
                    var conditions = response.Result.GetType().GetProperty("Conditions").GetValue(response.Result, null);
                    if(conditions != null)
                    {
                        foreach (var condition in conditions)
                        {
                            medical.Condictions.Add(new ConditionViewModel()
                            {
                                Id = condition.GetType().GetProperty("Id").GetValue(condition, null),
                                Description = condition.GetType().GetProperty("Description").GetValue(condition, null),
                                Type = (int) condition.GetType().GetProperty("Type").GetValue(condition, null),
                                MedicalHistoryId =condition.GetType().GetProperty("MedicalHistoryId").GetValue(condition, null)
                            });
                        }
                    }
                    var medications = response.Result.GetType().GetProperty("Medications").GetValue(response.Result, null);
                    if (medications != null)
                    {
                        foreach (var medication in medications)
                        {
                            medical.Medications.Add(new MedicationViewModel()
                            {
                                Id = medication.GetType().GetProperty("Id").GetValue(medication, null),
                                Description = medication.GetType().GetProperty("Description").GetValue(medication, null),
                                MedicalHistoryId = medication.GetType().GetProperty("MedicalHistoryId").GetValue(medication, null)
                            });
                        }
                    }
                    var treatments = response.Result.GetType().GetProperty("Treatments").GetValue(response.Result, null);
                    if (treatments != null)
                    {
                        foreach (var treatment in treatments)
                        {
                            medical.Treatments.Add(new TreatmentViewModel()
                            {
                                Id = treatment.GetType().GetProperty("Id").GetValue(treatment, null),
                                Description = treatment.GetType().GetProperty("Description").GetValue(treatment, null),
                                MedicalHistoryId = treatment.GetType().GetProperty("MedicalHistoryId").GetValue(treatment, null),
                                Date = (treatment.GetType().GetProperty("Date").GetValue(treatment, null)).ToString("dd'/'MM'/'yyyy"),
                                Result = treatment.GetType().GetProperty("Result").GetValue(treatment, null),
                                AttachmentName = treatment.GetType().GetProperty("AttachmentName").GetValue(treatment, null)
                            });
                        }
                    }
                }
                return View(medical);
            }
            return Redirect(Url.Action("Index", "Home"));
        }
        public async Task<ActionResult> ShowImage(int id = 0)
        {
            var respond = await _treatmentService.GetTreatmentById(id);
            var treatment = new TreatmentImageViewModel();
            treatment.Image = Convert.ToBase64String(respond.Result.Attachment);
            return View(treatment);
        }
        //This id is PatientId
        [ReferrOnly]
        public async Task<ActionResult> Print(int id = 0)
        {
            ModelState.Clear();
            if (id > 0)
            {
                var response = await _medicalHistoryService.GetorCreateMedicalByPatientId(id);
                var medical = MedicalHistoryViewModel.getMedical();
                if (response.Result != null)
                {
                    medical.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
                    medical.PDI = response.Result.GetType().GetProperty("PDI").GetValue(response.Result, null);
                    medical.GI = response.Result.GetType().GetProperty("GI").GetValue(response.Result, null);
                    medical.PlI = response.Result.GetType().GetProperty("PlI").GetValue(response.Result, null);
                    medical.SHI = response.Result.GetType().GetProperty("SHI").GetValue(response.Result, null);
                    medical.PatientId = response.Result.GetType().GetProperty("PatientId").GetValue(response.Result, null);
                    //fetching the patient
                    var patient = response.Result.GetType().GetProperty("Patient").GetValue(response.Result, null);
                    if (patient != null)
                    {
                        medical.Patient = new UpdatePatientViewModel()
                        {
                            Id = patient.GetType().GetProperty("Id").GetValue(patient, null),
                            Name = patient.GetType().GetProperty("Name").GetValue(patient, null),
                            LastName = patient.GetType().GetProperty("LastName").GetValue(patient, null),
                            Birthday = (patient.GetType().GetProperty("Birthday").GetValue(patient, null)).ToString("dd'/'MM'/'yyyy"),
                            Email = patient.GetType().GetProperty("Email").GetValue(patient, null),
                        };

                        var phones = patient.GetType().GetProperty("Phones").GetValue(patient, null);
                        foreach (var phone in phones)
                        {
                            if (phone.GetType().GetProperty("Primary").GetValue(phone, null) == 1)
                            {
                                medical.Patient.PhoneNumber = phone.GetType().GetProperty("Number").GetValue(phone, null);
                                medical.Patient.PhoneType = phone.GetType().GetProperty("Type").GetValue(phone, null);
                            }
                        }
                    }
                    var conditions = response.Result.GetType().GetProperty("Conditions").GetValue(response.Result, null);
                    if (conditions != null)
                    {
                        foreach (var condition in conditions)
                        {
                            medical.Condictions.Add(new ConditionViewModel()
                            {
                                Id = condition.GetType().GetProperty("Id").GetValue(condition, null),
                                Description = condition.GetType().GetProperty("Description").GetValue(condition, null),
                                Type = (int)condition.GetType().GetProperty("Type").GetValue(condition, null),
                                MedicalHistoryId = condition.GetType().GetProperty("MedicalHistoryId").GetValue(condition, null)
                            });
                        }
                    }
                    var medications = response.Result.GetType().GetProperty("Medications").GetValue(response.Result, null);
                    if (medications != null)
                    {
                        foreach (var medication in medications)
                        {
                            medical.Medications.Add(new MedicationViewModel()
                            {
                                Id = medication.GetType().GetProperty("Id").GetValue(medication, null),
                                Description = medication.GetType().GetProperty("Description").GetValue(medication, null),
                                MedicalHistoryId = medication.GetType().GetProperty("MedicalHistoryId").GetValue(medication, null)
                            });
                        }
                    }
                    var treatments = response.Result.GetType().GetProperty("Treatments").GetValue(response.Result, null);
                    if (treatments != null)
                    {
                        foreach (var treatment in treatments)
                        {
                            medical.Treatments.Add(new TreatmentViewModel()
                            {
                                Id = treatment.GetType().GetProperty("Id").GetValue(treatment, null),
                                Description = treatment.GetType().GetProperty("Description").GetValue(treatment, null),
                                MedicalHistoryId = treatment.GetType().GetProperty("MedicalHistoryId").GetValue(treatment, null),
                                Date = (treatment.GetType().GetProperty("Date").GetValue(treatment, null)).ToString("dd'/'MM'/'yyyy"),
                                Result = treatment.GetType().GetProperty("Result").GetValue(treatment, null)
                            });
                        }
                    }
                }
                return View(medical);
            }
            return Redirect(Url.Action("Index", "Home"));
        }

        public async Task<JsonResult> UpdateOralAdequacyIndexes(UOAIMedicalHistoryViewModel OAIndexes)
        {
            var response = await _medicalHistoryService.Update(OAIndexes.Id, OAIndexes.PatientId, OAIndexes.PDI, OAIndexes.GI, OAIndexes.PlI, OAIndexes.SHI);
            return Json(OAIndexes, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> AddCondition(ConditionViewModel condiction)
        {
            try
            {
                var response = await _conditionService.AddCondition(condiction.MedicalHistoryId, condiction.Description, condiction.Type);

                condiction.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
            }
            catch (Exception exception)
            {
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
            return Json(condiction, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> AddMedication(MedicationViewModel medication)
        {
            var response = await _medicationService.AddMedication(medication.MedicalHistoryId, medication.Description);
            medication.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
            return Json(medication, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AddTreatment(TreatmentViewModel treatment)
        {

            var response = await _treatmentService.AddTreatment(treatment.MedicalHistoryId,treatment.Date, treatment.Description,treatment.Result,treatment.Attachment);
            treatment.Id = response.Result.GetType().GetProperty("Id").GetValue(response.Result, null);
            treatment.AttachmentName = response.Result.GetType().GetProperty("AttachmentName").GetValue(response.Result, null);
            treatment.Attachment = null;
            return Json(treatment, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteMedication(MedicationViewModel medication)
        {
            var response = await _medicationService.DeleteMedication(medication.Id);
            return Json(medication, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteCondition(ConditionViewModel condiction)
        {
            var response = await _conditionService.DeleteCondition(condiction.Id);
            return Json(condiction, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteTreatment(TreatmentViewModel treatment) 
        {
            var response = await _treatmentService.DeleteTreatment(treatment.Id);
            return Json(treatment, JsonRequestBehavior.AllowGet);
        }
    }
}