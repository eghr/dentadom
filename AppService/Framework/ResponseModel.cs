﻿using System;
using System.Collections.Generic;

namespace AppService.Framework
{
    public class ResponseModel<TModel> : ResponseModel
    {
        public TModel Data { get; set; }
        public ResponseModel()
        {
            Data = Activator.CreateInstance<TModel>();
        }
        public void SetResult(TModel result)
        {
            base.SetResult(result);
        }
    }

    public class ResponseModel
    {
        public dynamic Result { get; set; }
        public bool ExcecutedSuccesfully { get; set; } = true;
        public List<string> Messages { get; set; }
        public string Href { get; set; }
        public string Function { get; set; }

        public void AddErrorMessages(List<string> errorMessages)
        {
            ExcecutedSuccesfully = false;

            if (Messages == null)
            {
                Messages =  new List<string>(errorMessages);
                return;
            }
            errorMessages.ForEach(x => Messages.Add(x));
        }
        public void AddErrorMessage(string message)
        {
            ExcecutedSuccesfully = false;

            if (Messages == null)
            {
                Messages = new List<string> {message};
                return;
            }
            Messages.Add(message);
        }
        public virtual void SetResult(dynamic result)
        {
            ExcecutedSuccesfully = true;
            Result = result;
        }
    }
}
