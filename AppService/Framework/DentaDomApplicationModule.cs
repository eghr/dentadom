﻿using Autofac;
using Data;
using Data.Framework;

namespace AppService.Framework
{
    public class DentaDomApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Database
            builder.RegisterType(typeof(DentaDomContext)).InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(IUnitOfWork).Assembly)
                .Where(service => !service.IsAbstract &&
                                  service.IsPublic)
                .AssignableTo(typeof(IUnitOfWork))
                .AsImplementedInterfaces();

            //Services

            builder.RegisterAssemblyTypes(typeof(IBaseService).Assembly)
                .Where(service => !service.IsAbstract &&
                                  service.IsPublic)
                .AssignableTo(typeof(IBaseService))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            base.Load(builder);
        }
    }
}