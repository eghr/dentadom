﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;
using System.Globalization;
using System.Web;
using Domain.Global.Convertions;

namespace AppService.Services
{
    public class TreatmentService : BaseService, ITreatmentService
    {
        public TreatmentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> AddTreatment(int medicalHistoryId, string date, string description, string result, HttpPostedFileBase attachment)
        {
            var response = new ResponseModel();
            try
            {
                var treatment = new Treatment()
                {
                    MedicalHistoryId = medicalHistoryId,
                    Date = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    Description = description,
                    Result = result
                };
                if (attachment != null)
                {
                    var image = FilesConvertion.ConvertToBytes(attachment);
                    treatment.Attachment = image.File;
                    treatment.AttachmentName = image.FileName;
                }
                UnitOfWork.Add(treatment);
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(treatment);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }

        public async Task<ResponseModel> DeleteTreatment(int id)
        {
            var response = new ResponseModel();
            var treatment = new Treatment()
            {
                Id = id 
            };
            UnitOfWork.Delete(treatment);
            await UnitOfWork.SaveChangesAsync();
            response.SetResult(treatment);
            response.ExcecutedSuccesfully = true;
            return response;
        }

        public async Task<ResponseModel> GetTreamentsByMedicalHistoryId(int id)
        {
            var response = new ResponseModel();
            var treatments = await UnitOfWork.Table<Treatment>().Where(x => x.MedicalHistoryId == id).ToListAsync();
            response.ExcecutedSuccesfully = true;
            response.SetResult(treatments);
            return response;
        }

        public async Task<ResponseModel<Treatment>> GetTreatmentById(int treatmentId)
        {
            var response = new ResponseModel<Treatment>();
            try
            {
                var treatment = await UnitOfWork.Table<Treatment>().Where(x => x.Id == treatmentId).FirstOrDefaultAsync();
                if (treatment != null)
                {
                    response.SetResult(treatment);
                    response.ExcecutedSuccesfully = true;
                }
                else
                {
                    response.AddErrorMessage("Imagen no Disponible");
                }
            }
            catch (Exception e)
            {
                response.AddErrorMessage("Imagen no Disponible");
            }
            return response;
        }
    }

    public interface ITreatmentService : IDisposable
    {
        Task<ResponseModel> AddTreatment(int medicalHistoryId, string date, string description, string result, HttpPostedFileBase attachment);
        Task<ResponseModel> GetTreamentsByMedicalHistoryId(int id);
        Task<ResponseModel> DeleteTreatment(int id);
        Task<ResponseModel<Treatment>> GetTreatmentById(int treatmentId);
    }
}
