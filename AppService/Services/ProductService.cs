﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;

namespace AppService.Services
{
    public class ProductService : BaseService, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> AddProduct(int productId, int quantity, int type)
        {
            var response = new ResponseModel();
            try
            {
                if (UnitOfWork.Table<Product>().Any(x => x.Id == productId))
                {
                    var product = await UnitOfWork.Table<Product>().Where(x => x.Id == productId).FirstOrDefaultAsync();
                    product.Quantity = product.Quantity + quantity;
                    var inventoryTransaction = new InventoryTransaction()
                    {
                        ProductId = productId,
                        Quantity = quantity,
                        Type = (TransactionType)type
                    };
                    UnitOfWork.Add(inventoryTransaction);
                    await UnitOfWork.SaveChangesAsync();
                    response.ExcecutedSuccesfully = true;
                }

            }
            catch (Exception)
            {

            }

            return response;
        }

        public async Task<ResponseModel> CreateProduct(string name, string brand, List<int> supplierId, double unitCost, int quantity)
        {
           var response = new ResponseModel();
            try
            {
                var product = new Product()
                {
                    Name = name,
                    Brand =brand,
                    UnitCost = unitCost,
                    Quantity =quantity,
                    Suppliers = new List<Supplier>()
            };
                foreach (int id in supplierId) {
                    var supplier = await UnitOfWork.Table<Supplier>().Where(x => x.Id == id).FirstOrDefaultAsync();
                    if (supplier != null) {
                        product.Suppliers.Add(supplier);
                    }
                }

                UnitOfWork.Add(product);
                await UnitOfWork.SaveChangesAsync();
                var inventoryTransaction = new InventoryTransaction()
                {
                    ProductId = product.Id,
                    Quantity = quantity,
                    Type = (TransactionType)2
                };
                UnitOfWork.Add(inventoryTransaction);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception e)
            {
                throw e;
            }

           return response;
        }

        public async Task<ResponseModel<List<Product>>> GetProducts()
        {
            var response = new ResponseModel<List<Product>> ();
            try
            {
                var products = await UnitOfWork.Table<Product>()
                                .Include(x => x.Suppliers)
                                 .ToListAsync();
                                    

                response.Data = products;
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception e)
            {
                throw e;
            }

            return response;
        }

        public async Task<ResponseModel> RemoveProduct(int productId, int quantity, int type)
        {
            var response = new ResponseModel();
            try
            {
                if(UnitOfWork.Table<Product>().Any(x => x.Id == productId))
                {
                    var product =await UnitOfWork.Table<Product>().Where(x => x.Id == productId).FirstOrDefaultAsync();
                    product.Quantity = product.Quantity - quantity;
                    var inventoryTransaction = new InventoryTransaction()
                    {
                        ProductId = productId,
                        Quantity = quantity,
                        Type = (TransactionType)type
                    };
                    UnitOfWork.Add(inventoryTransaction);
                    await UnitOfWork.SaveChangesAsync();
                    response.ExcecutedSuccesfully = true; 
                }
                
            }
            catch (Exception)
            {

            }

            return response;
        }

        public async Task<ResponseModel<Product>> UpdateProduct(int id, string name, string brand, List<int> supplierId, double unitCost)
        {
            var response = new ResponseModel<Product>();
            try
            {
                var product = await UnitOfWork.Table<Product>().Where(x => x.Id == id).Include(x =>x.Suppliers).FirstOrDefaultAsync();
                product.Name = name;
                product.Brand = brand;
                product.UnitCost = unitCost;
                var suppliers = new List<Supplier>();
                foreach (var sup in supplierId) {
                    var supplier = await UnitOfWork.Table<Supplier>().Where(x => x.Id == sup).FirstOrDefaultAsync();
                    if (supplier != null) {
                        suppliers.Add(supplier);
                    }
                }
                product.Suppliers = suppliers;
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = product;
            }
            catch (Exception e)
            {
                response.AddErrorMessage("error de coneccion de a la base de datos");
            }
            return response;
        }
    }

    public interface IProductService : IDisposable
    {
        Task<ResponseModel> CreateProduct(string name, string brand, List<int> supplierId, double unitCost, int quantity);
        Task<ResponseModel> AddProduct(int productId, int quantity, int type);
        Task<ResponseModel> RemoveProduct(int productId, int quantity, int type);
        Task<ResponseModel<List<Product>>> GetProducts();
        Task<ResponseModel<Product>> UpdateProduct(int id, string name, string brand, List<int> supplierId, double unitCost);
    }
}
