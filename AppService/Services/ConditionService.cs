﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;

namespace AppService.Services
{
    public class ConditionService : BaseService, IConditionService
    {
        public ConditionService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public async Task<ResponseModel> AddCondition(int medicalHistoryId, string description, int type)
        {
            var response = new ResponseModel();
            try
            {
                var condition = new Condition()
                {
                    MedicalHistoryId = medicalHistoryId,
                    Description = description,
                    Type = (ConditionType)type
                };

                UnitOfWork.Add(condition);
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(condition);
                return response;
            }
            catch (Exception exception)
            {
                response.AddErrorMessage("Occurio un Error Guardando esta Condicion");
                response.AddErrorMessage(exception.Message);
                return response;
            }
        }

        public async Task<ResponseModel> DeleteCondition(int id)
        {
            var response = new ResponseModel();
            UnitOfWork.Delete(new Condition() { Id = id });
            await UnitOfWork.SaveChangesAsync();
            return response;
        }
    }
    public interface IConditionService:IDisposable
    {
        Task<ResponseModel> AddCondition(int medicalHistoryId, string description, int type);
        Task<ResponseModel> DeleteCondition(int id);
    }
}
