﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppService.Framework;
using Data.Entities;
using Data.Framework;

namespace AppService.Services
{
    public class MedicalProcedureService : BaseService, IMedicalProcedureService
    {
        public MedicalProcedureService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel<MedicalProcedure>> CreateAsync(string description, decimal price)
        {
            var response = new ResponseModel<MedicalProcedure>();
            try
            {
                var medicalProcedure = new MedicalProcedure() { Description = description, Price = price };
                UnitOfWork.Add(medicalProcedure);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = medicalProcedure;
            }
            catch (Exception)
            {
                response.ExcecutedSuccesfully = false;
            }
            return response;
        }

        public async Task<ResponseModel<MedicalProcedure>> DeleteAsync(int id)
        {
            var response = new ResponseModel<MedicalProcedure>();
            try
            {
                var medicalProcedure = new MedicalProcedure() { Id = id };
                UnitOfWork.Delete(medicalProcedure);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = medicalProcedure;
            }
            catch (Exception)
            {
                response.ExcecutedSuccesfully = false;
            }
            return response;
        }

        public async Task<ResponseModel<MedicalProcedure>> UpdateAsync(int id, string description, decimal price)
        {
            var response = new ResponseModel<MedicalProcedure>();
            try
            {
                var medicalProcedure = new MedicalProcedure() { Id = id, Description = description, Price = price };
                UnitOfWork.Update(medicalProcedure);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = medicalProcedure;
            }
            catch (Exception)
            {
                response.ExcecutedSuccesfully = false;
            }
            return response;
        }

        public async Task<ResponseModel<List<MedicalProcedure>>> GetAllAsync()
        {
            var result = new ResponseModel<List<MedicalProcedure>>();

            var data = await UnitOfWork.Table<MedicalProcedure>().ToListAsync();

            result.Data = data;

            return result;
        }
    }

    public interface IMedicalProcedureService : IDisposable
    {
        Task<ResponseModel<List<MedicalProcedure>>> GetAllAsync();
        Task<ResponseModel<MedicalProcedure>> CreateAsync(string description, decimal price);
        Task<ResponseModel<MedicalProcedure>> DeleteAsync(int id);
        Task<ResponseModel<MedicalProcedure>> UpdateAsync(int id, string description, decimal price);
    }
}
