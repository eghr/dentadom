﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;

namespace AppService.Services
{
    public class MedicationServise : BaseService, IMedicationServise
    {
        public MedicationServise(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> AddMedication(int medicalHistoryId, string description)
        {
            var response = new ResponseModel();
            var medication = new Medication()
            {
                Description = description,
                MedicalHistoryId = medicalHistoryId
            };
            try
            {
                UnitOfWork.Add(medication);
                await UnitOfWork.SaveChangesAsync();
                response.ExcecutedSuccesfully = true;
                response.SetResult(medication);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.SetResult(false);
            }
            return response;
        }

        public async Task<ResponseModel> DeleteMedication(int id)
        {
           var response = new ResponseModel();
           var medication = new Medication() { Id = id };
           UnitOfWork.Delete(medication);
           await UnitOfWork.SaveChangesAsync();
           return response;
        }
    }

    public interface IMedicationServise : IDisposable
    {
        Task<ResponseModel> AddMedication(int medicalHistoryId, string description);
        Task<ResponseModel> DeleteMedication(int id);
    }
}
