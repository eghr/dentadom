﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using AppService.Framework;
using Data.Entities;
using Data.Framework;
using Domain.Global.Dto;
using Domain.Global.Enums;
using Reports.Helper;
using Reports.Reports.Invoice;

namespace AppService.Services
{
    public class InvoiceService : BaseService, IInvoiceService
    {
        public InvoiceService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel<int>> CreateInvoiceAsync(Invoice invoice)
        {
            var result = new ResponseModel<int>();

            if (invoice.InvoiceDetails == null || !invoice.InvoiceDetails.Any())
            {
                result.AddErrorMessage("Ocurrio un Error tratando de Crear la Factura");
                return result;
            }
            try
            {
                using (var transactionScop = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    UnitOfWork.Table<Invoice>().Add(invoice);
                    await UnitOfWork.SaveChangesAsync();

                    var appointmet = await UnitOfWork.Table<Appointment>()
                        .FirstOrDefaultAsync(x => x.Id.Equals(invoice.AppointmentId));

                    if (appointmet == null)
                    {
                        result.AddErrorMessage("La cita a la que desea registrarle un pago no existe");
                        return result;
                    }

                    appointmet.IsPaid = true;

                    UnitOfWork.Update(appointmet);
                    await UnitOfWork.SaveChangesAsync();

                    var partialResult = await CreateCreditInvoice(new CreditInvoice
                    {
                        InvoiceId = invoice.Id,
                        Balance = invoice.InvoiceDetails.Sum(x => x.MedicalProcedurePrice)
                    });

                    if (!partialResult.ExcecutedSuccesfully)
                    {
                        result.AddErrorMessages(partialResult.Messages);
                        return result;
                    }

                    transactionScop.Complete();
                    result.Data = invoice.Id;
                }
            }
            catch (Exception exception)
            {
                result.AddErrorMessage(exception.Message);
                return result;
            }

            return result;
        }


        private async Task<ResponseModel<Invoice>> CreateCreditInvoice(CreditInvoice creditInvoice)
        {
            var result = new ResponseModel<Invoice>();

            try
            {
                UnitOfWork.Add(creditInvoice);
                await UnitOfWork.SaveChangesAsync();
            }
            catch (Exception e)
            {
                result.AddErrorMessage(e.Message);
                return result;
            }

            return result;
        }

        public async Task<ResponseModel<List<CreditInvoice>>> GetAllCreditInvoice()
        {
            var result = new ResponseModel<List<CreditInvoice>>();

            result.Data = await UnitOfWork.Table<CreditInvoice>()
                .Include(x => x.Invoice)
                .Include(x => x.Invoice.Patient)
                .Where(x => !x.IsDeleted && x.Balance > 0 )
                .ToListAsync();

            return result;
        }

        public async Task<ResponseModel<CreditInvoice>> GetCreditInvoiceById(int creditInvoiceId)
        {
            var result = new ResponseModel<CreditInvoice>();

            result.Data = await UnitOfWork.Table<CreditInvoice>()
                .Include(x => x.Invoice)
                .Include(x => x.Invoice.InvoiceDetails)
                .Include(x => x.Invoice.Patient)
                .Include(m => m.Invoice.InvoiceDetails.Select(x => x.MedicalProcedure))
                .Where(x => !x.IsDeleted && x.Id == creditInvoiceId && x.Balance > 0)
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ResponseModel<Invoice>> GetInvoiceByAppointmentId(int appointmentId)
        {
            var result = new ResponseModel<Invoice>();
            try
            {
                var data = await UnitOfWork.Table<Invoice>().Where(x => x.AppointmentId == appointmentId)
                    .FirstOrDefaultAsync();
                result.Data = data;
            }
            catch (Exception)
            {
                result.AddErrorMessage("No se encontro la factura");
            }
            return result;
        }

        public async Task<ResponseModel<List<Invoice>>> GetInvoiceByDateInterval(DateTime firstInterval,
            DateTime lastInterval)
        {
            var response = new ResponseModel<List<Invoice>>();
            try
            {
                var invoices = await UnitOfWork.Table<Invoice>()
                    //.Where(x => x.CreatedOn > firstInterval)
                    //.Where(x => x.CreatedOn < lastInterval)
                    .Include(x => x.InvoiceDetails)
                    .Include(x => x.Appointment)
                    .Where(x => x.Appointment.StartDate > firstInterval)
                    .Where(x => x.Appointment.StartDate < lastInterval)
                    .ToListAsync();
                response.ExcecutedSuccesfully = true;
                response.Data = invoices;
            }
            catch (Exception exception)
            {
                response.AddErrorMessage(exception.Message);
            }
            return response;
        }

        public async Task<ResponseModel<Invoice>> GetInvoiceById(int invoiceId)
        {
            var result = new ResponseModel<Invoice>();
            var data = await UnitOfWork.Table<Invoice>().Include(x => x.User)
                .Include(x => x.Patient).Include(x => x.Patient.Phones)
                .Include(x => x.InvoiceDetails.Select(y => y.MedicalProcedure))
                .Include(x => x.Appointment).Where(x => x.Id == invoiceId).FirstOrDefaultAsync();

            result.Data = data;

            return result;
        }

        public async Task<ResponseModel<List<InvoiceReportDto>>> GetByDatesInterval(InvoiceReportDto criteria)
        {
            var result = new ResponseModel<List<InvoiceReportDto>>();

            criteria.ToDateAsDateTime = Convert.ToDateTime(criteria.ToDate, CultureInfo.InvariantCulture);
            criteria.FromDateAsDateTime = Convert.ToDateTime(criteria.FromDate, CultureInfo.InvariantCulture);

            try
            {
                var data = await UnitOfWork.Table<InvoiceDetail>()
                    .Include(x => x.Invoice.Patient)
                    .Include(x => x.Invoice)
                    .Include(x => x.MedicalProcedure)
                    .Where(x => !x.Invoice.IsDeleted)
                    .Where(x => x.Invoice.CreatedOn >= criteria.FromDateAsDateTime &&
                                x.Invoice.CreatedOn <= criteria.ToDateAsDateTime)
                    .Select(x => new InvoiceReportDto
                    {
                        InvoiceNumber = x.Invoice.Id.ToString(),
                        InvoiceAsDateTime = x.CreatedOn,
                        CustomerName = x.Invoice.Patient.Name + " " + x.Invoice.Patient.LastName,
                        Price = x.MedicalProcedurePrice,
                        Discount = x.Invoice.Discount,
                        Tax = x.Invoice.Tax,
                        ClinicalProcedure = x.MedicalProcedure.Description,
                        FromDate = criteria.FromDate,
                        ToDate = criteria.ToDate
                    })
                    .ToListAsync();


                result.Data = data;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return result;
        }

        public async Task<ResponseModel<Invoice>> DeleteInvoiceByAppointmentId(int appointmentId)
        {
            var result = new ResponseModel<Invoice>();

            try
            {
                var data = await UnitOfWork.Table<Invoice>()
                        .Where(x=>x.AppointmentId == appointmentId)
                        .Include(x => x.InvoiceDetails)
                        .FirstOrDefaultAsync();
                if (data != null) {
                    var credit = await UnitOfWork.Table<CreditInvoice>()
                        .Where(x => x.InvoiceId == data.Id)
                        .FirstOrDefaultAsync();

                    if (credit != null) {
                        var reciepts = await UnitOfWork.Table<IncomingReceipt>()
                        .Where(x => x.CreditInvoiceId== credit.Id )
                        .ToListAsync();
                        foreach (var receipt in reciepts.ToList())
                        {
                            UnitOfWork.Delete(receipt);
                        }
                        UnitOfWork.Delete(credit);
                    }
                    if (data.InvoiceDetails.Count > 0)
                    {
                        foreach (var details in data.InvoiceDetails.ToList()) {
                            UnitOfWork.Delete(details);
                        }
                    }
                    UnitOfWork.Delete(data);
                    var appointment = await UnitOfWork.Table<Appointment>()
                                                    .Where(x => x.Id == appointmentId)
                                                    .FirstOrDefaultAsync();
                    if(appointment!= null)
                    {
                        appointment.IsPaid = false;
                    }
                }
                await UnitOfWork.SaveChangesAsync();
               result.ExcecutedSuccesfully = true;
            }
            catch (Exception e)
            {
                result.AddErrorMessage("");
            }
            return result;
        }

        public async Task<ReportResponse> GetInvoiceReport<TData>(TData data) where TData : class
        {
            var response = ReportGenerator.GetReportFile<InvoiceReport, TData>(data);

            return response;
        }

        public async Task<ReportResponse> GetDetailedInvoiceReport<TData>(TData data) where TData : class
        {
            var response = ReportGenerator.GetReportFile<InvoiceReportDetailed, TData>(data);

            return response;
        }

        
    }

    public interface IInvoiceService : IBaseService
    {
        Task<ResponseModel<int>> CreateInvoiceAsync(Invoice invoice);
        Task<ResponseModel<Invoice>> GetInvoiceById(int invoiceId);
        Task<ResponseModel<Invoice>> GetInvoiceByAppointmentId(int appointmentId);
        Task<ResponseModel<List<Invoice>>> GetInvoiceByDateInterval(DateTime firstInterval, DateTime lastInterval);
        Task<ResponseModel<List<InvoiceReportDto>>> GetByDatesInterval(InvoiceReportDto criteria);
        Task<ReportResponse> GetInvoiceReport<TData>(TData data) where TData : class;
        Task<ReportResponse> GetDetailedInvoiceReport<TData>(TData data) where TData : class;
        Task<ResponseModel<List<CreditInvoice>>> GetAllCreditInvoice();
        Task<ResponseModel<CreditInvoice>> GetCreditInvoiceById(int creditInvoiceId);
        Task<ResponseModel<Invoice>> DeleteInvoiceByAppointmentId(int appointmentId);
    }
}
