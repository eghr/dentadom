﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Transactions;
using AppService.Framework;
using Data.Entities;
using Data.Framework;

namespace AppService.Services
{
    public class IncomingReceiptService : BaseService, IIncomingReceiptService
    {
        public IncomingReceiptService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel<IncomingReceipt>> CreateNewAsync(IncomingReceipt incomingReceipt)
        {
            var result = new ResponseModel<IncomingReceipt>();

            if (incomingReceipt == null)
            {
                result.AddErrorMessage("Debe llenar los datos pertienentes al recibo de ingreso");
                return result;
            }

            using (var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    var creditInvoiceToUpdate = await UnitOfWork.Table<CreditInvoice>()
                        .FirstOrDefaultAsync(x => x.Id == incomingReceipt.CreditInvoiceId);

                    if (creditInvoiceToUpdate == null)
                    {
                        result.AddErrorMessage(
                            "La factura especificada no existe o no pudo ser encontrada en el sistema.");
                        return result;
                    }

                    if (incomingReceipt.Amount > creditInvoiceToUpdate.Balance)
                    {
                        result.AddErrorMessage("El importe no puede ser mayor que el balance adeudado");
                        return result;
                    }

                    creditInvoiceToUpdate.Balance -= incomingReceipt.Amount;

                    UnitOfWork.Add(incomingReceipt);
                    UnitOfWork.Update(creditInvoiceToUpdate);

                    await UnitOfWork.SaveChangesAsync();

                    transactionScope.Complete();

                    return result;
                }

                catch (Exception e)
                {
                    transactionScope.Dispose();
                    result.AddErrorMessage(e.Message);
                    return result;
                }
            }
        }
    }

    public interface IIncomingReceiptService : IBaseService
    {
        Task<ResponseModel<IncomingReceipt>> CreateNewAsync(IncomingReceipt incomingReceipt);
    }
}
