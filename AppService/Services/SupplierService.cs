﻿using AppService.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Framework;
using Data.Entities;
using System.Data.Entity;

namespace AppService.Services
{
    public class SupplierService : BaseService, ISupplierService
    {
        public SupplierService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<ResponseModel> CreateSupplier(string name, string phone, string address)
        {
            var response = new ResponseModel();
            try
            {
                var supplier = new Supplier()
                {
                    Name = name,
                    Phone = phone,
                    Address = address
                };
                UnitOfWork.Add(supplier);
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(supplier);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }

        public async Task<ResponseModel> GetSuppliers()
        {
            var response = new ResponseModel();
            try
            {
                var suppliers = await UnitOfWork.Table<Supplier>().Where(x => x.IsDeleted == false).ToListAsync();
                response.SetResult(suppliers);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }

        public async Task<ResponseModel> SoftDeleteSupplier(int supplierId)
        {
            var response = new ResponseModel();

            try
            {
                var supplier = await UnitOfWork.Table<Supplier>().Where(x => x.Id == supplierId).FirstOrDefaultAsync();
                supplier.IsDeleted = true;
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(supplier);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }

        public async Task<ResponseModel> UpdateSupplier(int id, string name, string phone, string address)
        {
            var response = new ResponseModel();

            try
            {
                var supplier = new Supplier()
                {
                    Id = id,
                    Name = name,
                    Phone = phone,
                    Address = address
                };
                UnitOfWork.Update(supplier);
                await UnitOfWork.SaveChangesAsync();
                response.SetResult(supplier);
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }
    }

    public interface ISupplierService : IDisposable
    {
        Task<ResponseModel> CreateSupplier(string name, string phone, string address);
        Task<ResponseModel> GetSuppliers();
        Task<ResponseModel> UpdateSupplier(int id, string name, string phone, string address);
        Task<ResponseModel> SoftDeleteSupplier(int supplierId);
    }
}
