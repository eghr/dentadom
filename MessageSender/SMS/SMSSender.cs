﻿using AppService.Framework;
using Data.Entities;
using MessageSender.SMS.SMSAPIRespond;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MessageSender.SMS
{
    public class SMSSender
    {
        private static HttpClient clientSMS = new HttpClient();
        private static bool Started = false;

        public void Start()
        {
            clientSMS.BaseAddress = new Uri("https://www.experttexting.com/ExptRestApi/sms/json");
            clientSMS.DefaultRequestHeaders.Accept.Clear();
            clientSMS.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            Started = true;
        }

        public async Task<ResponseModel> SendSMS(SMSMessage msg)
        {
            if (!Started) Start();
            var response = new ResponseModel();
            var message = msg;
            string path = "/ExptRestApi/sms/json/Message/Send";
            APIResponse result = null;
            try
            {
                HttpResponseMessage httpResponse = await clientSMS.PostAsJsonAsync(path, message);
                if (httpResponse.IsSuccessStatusCode)
                {
                    result = await httpResponse.Content.ReadAsAsync<APIResponse>();
                    response.ExcecutedSuccesfully = true;
                }
                else
                {
                    response.ExcecutedSuccesfully = false;
                }
            }
            catch (Exception e)
            {
                response.ExcecutedSuccesfully = false;
            }
            return response;
        }

    }
}
