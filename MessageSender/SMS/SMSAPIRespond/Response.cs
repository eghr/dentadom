using System;
using System.Collections.Generic;
using System.Linq;

namespace MessageSender.SMS.SMSAPIRespond
{
    public class Response
    {
        public string message_id { get; set; }
        public int message_count { get; set; }
        public double price { get; set; }
        public double Balance { get; set; }
    }
}
