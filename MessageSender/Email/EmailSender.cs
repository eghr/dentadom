﻿using AppService.Framework;
using Data.Entities;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace MessageSender.Email
{
    public class EmailSender
    {
        private static GmailService service;
        private static string[] Scopes = { GmailService.Scope.GmailSend, GmailService.Scope.GmailCompose, GmailService.Scope.GmailReadonly };
        private static string ApplicationName = "Dentadom RD";
        private static bool Started = false;

        private void Start()
        {
            UserCredential credential;
            using (var stream = new FileStream(HostingEnvironment.MapPath("~/GoogleAPIConfig/client_secret.json"), FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials\\gmail-dotnet-quickstart.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                            GoogleClientSecrets.Load(stream).Secrets,
                            Scopes,
                            "user",
                            CancellationToken.None,
                            new FileDataStore(credPath, true)).Result;
            }
            
            // Create Gmail API service.
            service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            Started = true;
        }
       
        public async Task<ResponseModel> SendEmail(AE.Net.Mail.MailMessage msg)
        {
            if (!Started) Start();
            var response = new ResponseModel();
            try
            {
                var message = msg;
                var msgStr = new StringWriter();
                message.Save(msgStr);
                var result = await service.Users.Messages.Send(new Message
                {
                    Raw = Base64UrlEncode(msgStr.ToString())
                }, "me").ExecuteAsync();
                response.ExcecutedSuccesfully = true;
            }
            catch (Exception)
            {
                response.ExcecutedSuccesfully = false;
            }
            return response;
  
        }
        private static string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
              .Replace('+', '-')
              .Replace('/', '_')
              .Replace("=", "");
        }
    }
}
