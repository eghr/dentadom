﻿using Data.Entities;
using MessageSender.Email;
using MessageSender.SMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MessageSender
{
    public class DentadomMessageManager
    {
        public static async Task sendReminders(List<Appointment> appoimentsToRemind)
        {
            foreach (var appointment in appoimentsToRemind)
            {
                if (!String.IsNullOrEmpty(appointment.Patient.Email) && !appointment.IsMessageSent)
                {
                    await SendEmailForAppointment(appointment);
                }
                if (appointment.Patient.Phones.FirstOrDefault(x => x.Type == "Celular") != null && !appointment.IsMessageSent)
                {
                    await SendSMSForAppoinment(appointment);
                }
            }
            

        }

        public static async Task SendEmailForAppointment(Appointment appointment)
        {
           await new EmailSender().SendEmail(MakeEmailFromAppointment(appointment));
        }

        public static async Task SendSMSForAppoinment(Appointment appointment)
        {
            await new SMSSender().SendSMS(MakeSMSFromAppointment(appointment));
        }

        private static AE.Net.Mail.MailMessage MakeEmailFromAppointment(Appointment reminder)
        {
            var msg = new AE.Net.Mail.MailMessage
            {
                Subject = "DentaDOM Co.",
                Body = "Buenas Tardes Señor " + reminder.Patient.CompleteName
                        + "\nLe contactamos para recordarle de su cita de hoy a las " + reminder.StartDate.ToString("hh: mm:ss")
                        + " con el doctor " + reminder.User.LastName +
                        ".\n\nLe deseamos un buen resto del dia, del Grupo Dental Polanco.",
                From = new MailAddress("DentadomRD@gmail.com")
            };
            msg.ReplyTo.Add(msg.From); // Bounces without this!!
            msg.To.Add(new MailAddress(reminder.Patient.Email));
            return msg;
        }

        private static SMSMessage MakeSMSFromAppointment(Appointment appointment)
        {
            var to = "1" + appointment.Patient.Phones.ElementAt(0).Number;
            var text = "Saludos " + appointment.Patient.CompleteName
                        + "\nLe contactamos para recordarle de su cita de hoy a las " + appointment.StartDate.ToString("hh: mm:ss")
                        + " con el doctor " + appointment.User.LastName +
                        ".\n\nLe deseamos un buen resto del dia, del Grupo Dental Polanco.";
            return new SMSMessage() { to = to, text = text, type = "text" };
        }
    }
}
