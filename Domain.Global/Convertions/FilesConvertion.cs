﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Domain.Global.Convertions
{
    public class FilesConvertion
    {
        public static FileDto ConvertToBytes(HttpPostedFileBase unConvertedFile)
        {
            var target = new MemoryStream();

            unConvertedFile.InputStream.CopyTo(target);

            var convertedFile = new FileDto
            {
                FileName = unConvertedFile.FileName,
                File = target.ToArray()
            };

            return convertedFile;
        }
    }
}
