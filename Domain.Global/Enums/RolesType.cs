﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Global.Enums
{
    public enum RoleTypeEnum
    {
        [Display(Name = "Administrador")] Administrator = 1,
        [Display(Name = "Odontologo")] Dentist,
        [Display(Name = "Secretaria")] Secretary,
        [Display(Name = "Asistente Dental")] DentalAssistant
    }
}
