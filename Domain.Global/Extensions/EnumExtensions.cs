﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Global.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum value)
        {
            var type = value.GetType();
            if (!type.IsEnum) return " ";

            var members = type.GetMember(value.ToString());
            if (members.Length == 0) return " ";

            var member = members[0];
            var attributes = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            if (attributes.Length == 0) return " ";

            var attribute = (DisplayAttribute)attributes[0];
            return attribute.GetName();
        }

        public static List<EnumParameter> GetAllDiplayNames(this Enum enumType)
        {
            var enumDisplayNames = new List<EnumParameter>();

            var values = Enum.GetValues(enumType.GetType());

            for (int i = 0; i < values.Length; i++)
            {
                var type = values.GetValue(i).GetType();

                var members = type.GetMember(values.GetValue(i).ToString());

                var member = members[0];

                var attributes = member.GetCustomAttributes(typeof(DisplayAttribute), false);

                var attribute = (DisplayAttribute)attributes[0];

                enumDisplayNames.Add(new EnumParameter
                {
                    Id = i+1,
                    DisplayName = attribute.GetName()
                });
            }

            return enumDisplayNames;
        }
    }
}