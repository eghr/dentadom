﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Domain.Global.Dto
{
    public class InvoiceReportDto
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate => InvoiceAsDateTime.ToShortDateString();
        public string CustomerName { get; set; }
        public string ClinicalProcedure { get; set; }
        public decimal Price { get; set; }
        public decimal Subtotal => Convert.ToDecimal(string.Format("{0:F2}", Price + (Discount<=0? 0:(Price * (Discount / 100)))));
        public decimal Discount { get; set; }
        public decimal Tax { get; set; }
        public decimal Total => Convert.ToDecimal(string.Format("{0:F2}",(Subtotal + (Subtotal *Tax/100))));
        [Display(Name = "Desde Fecha")]
        public string FromDate { get; set; }
        [Display(Name = "Hasta Fecha")]
        public string ToDate { get; set; }
        public DateTime? FromDateAsDateTime { get; set; }
        public DateTime? ToDateAsDateTime { get; set; }

        public DateTime InvoiceAsDateTime { get; set; }
    }
}
